package com.app.neoris.puestosit;

import android.app.Application;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.text.TextWatcher;
import android.view.inputmethod.EditorInfo;

import com.app.neoris.puestosit.model.App;
import com.app.neoris.puestosit.model.Puesto;
import com.app.neoris.puestosit.model.PuestoAdapter;
import com.app.neoris.puestosit.model.Puesto_;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.query.Query;
import android.widget.TextView.OnEditorActionListener;

public class PuestosITActivity extends AppCompatActivity {

    private EditText editText;
    private View addNoteButton;

    private Box<Puesto> puestosBox;
    private Query<Puesto> puestosQuery;
    private PuestoAdapter puestoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /*
        // Configuracion personalizada de librería Toasty.
        Toasty.Config.getInstance()
            .setErrorColor(@ColorInt int errorColor) // optional
            .setInfoColor(@ColorInt int infoColor) // optional
            .setSuccessColor(@ColorInt int successColor) // optional
            .setWarningColor(@ColorInt int warningColor) // optional
            .setTextColor(@ColorInt int textColor) // optional
            .tintIcon(boolean tintIcon) // optional (apply textColor also to the icon)
            .setToastTypeface(@NonNull Typeface typeface) // optional
            .setTextSize(int sizeInSp) // optional
            .apply(); // required
        */

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puestos_it);
        setUpViews();

        BoxStore boxStore = ((App) getApplication()).getBoxStore();
        puestosBox = boxStore.boxFor(Puesto.class);

        puestosQuery = puestosBox.query().order(Puesto_.nombreOcupante).build();
        updateNotes();

        // Prueba del uso de librería Toasty
        Toasty.success(this.getBaseContext(), "ÉXITO!", Toast.LENGTH_SHORT, true).show();


    }

    private void updateNotes() {
        List<Puesto> puestos = puestosQuery.find();
        puestoAdapter.setPuestos(puestos);
    }

    protected void setUpViews() {
        ListView listView = (ListView) findViewById(R.id.listViewNotes);
        listView.setOnItemClickListener(puestoClickListener);

        puestoAdapter = new PuestoAdapter();
        listView.setAdapter(puestoAdapter);

        addNoteButton = findViewById(R.id.buttonAdd);
        addNoteButton.setEnabled(false);

        editText = (EditText) findViewById(R.id.editTextNote);
        editText.setOnEditorActionListener(new OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    addNote();
                    return true;
                }
                return false;
            }
        });
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                boolean enable = s.length() != 0;
                addNoteButton.setEnabled(enable);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }

    public void onAddButtonClick(View view) {
        addNote();
    }

    private void addNote() {
        String nombreOcupante = editText.getText().toString();
        editText.setText("");

        DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
        String comment = "Added on " + df.format(new Date());

        Puesto puesto = new Puesto();
        puesto.setNombreOcupante(nombreOcupante);
//        puesto.setComment(comment);
//        puesto.setDate(new Date());
        puestosBox.put(puesto);
        Log.d(App.TAG, "Inserted new note, ID: " + puesto.getId());

        updateNotes();
    }

    AdapterView.OnItemClickListener puestoClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Puesto puesto = puestoAdapter.getItem(position);
            puestosBox.remove(puesto);
            Log.d(App.TAG, "Deleted note, ID: " + puesto.getId());
            updateNotes();
        }
    };
}

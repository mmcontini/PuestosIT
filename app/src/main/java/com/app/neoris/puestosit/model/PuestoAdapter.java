package com.app.neoris.puestosit.model;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.neoris.puestosit.R;

import java.util.ArrayList;
import java.util.List;

/**
 *  Adaptador necesario para ORM ObjectBox.
 */
public class PuestoAdapter extends BaseAdapter {

    private List<Puesto> dataset;

    private static class PuestoViewHolder {

        public TextView text;
        public TextView comment;

        public PuestoViewHolder(View itemView) {
            text = (TextView) itemView.findViewById(R.id.textViewNoteText);
            comment = (TextView) itemView.findViewById(R.id.textViewNoteComment);
        }
    }

    public PuestoAdapter() {
        this.dataset = new ArrayList<>();
    }

    public void setPuestos(List<Puesto> puestos) {
        dataset = puestos;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PuestoViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_note, parent, false);
            holder = new PuestoViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (PuestoViewHolder) convertView.getTag();
        }

        Puesto puesto = getItem(position);
        holder.text.setText(puesto.getNombreOcupante());

//        Note note = getItem(position);
//        holder.text.setText(note.getText());
//        holder.comment.setText(note.getComment());

        return convertView;
    }

    @Override
    public int getCount() {
        return dataset.size();
    }

    @Override
    public Puesto getItem(int position) {
        return dataset.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
package com.app.neoris.puestosit.model;

import java.util.Date;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

/**
 * Representacion de entidad para datos de un puesto.
 */
@Entity
public class Puesto {

    @Id
    private long id;

    private String nombreOcupante;
    private String zona;
    private Date fecha;
    private boolean laptop;
    private boolean desktop;
    private boolean telefono;

    /**
     *  Constructor arg.
     *  @param id
     *  @param nombreOcupante
     *  @param zona
     *  @param fecha
     *  @param laptop
     *  @param desktop
     *  @param telefono
     */
    public Puesto(
            long id, String nombreOcupante, String zona, Date fecha, boolean laptop, boolean desktop, boolean telefono) {
        this.id = id;
        this.nombreOcupante = nombreOcupante;
        this.zona = zona;
        this.fecha = fecha;
        this.laptop = laptop;
        this.desktop = desktop;
        this.telefono = telefono;
    }

    /**
     *  Constructor no-arg
     */
    public Puesto() {
        super();
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombreOcupante() {
        return this.nombreOcupante;
    }

    public void setNombreOcupante(String nombreOcupante) {
        this.nombreOcupante = nombreOcupante;
    }

    public String getZona() {
        return this.zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public boolean isLaptop() {
        return this.laptop;
    }

    public void setLaptop(boolean laptop) {
        this.laptop = laptop;
    }

    public boolean isDesktop() {
        return desktop;
    }

    public void setDesktop(boolean desktop) {
        this.desktop = desktop;
    }

    public boolean isTelefono() {
        return this.telefono;
    }

    public void setTelefono(boolean telefono) {
        this.telefono = telefono;
    }

}
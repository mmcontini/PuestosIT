package com.app.neoris.puestosit.constants;

/**
 *  Constantes de la BBDD.
 */
public class ConstantesBBDD {

    //General
    public static final String DB_NAME = "puestos.db";
    public static final int DB_VERSION = 1;

    //TABLA CLIENTES
    public static final String TABLA_PUESTOS = "puestos";

}